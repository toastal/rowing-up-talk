---
title: Rowing Up with PureScript’s Row Types
---

[![PureScript logo](./assets/purescript-logo.svg)][purescript]

## Rowing Up with PureScript’s Row Types

Kaidao 🍳 ไข่ดาว

<small><time datetime="2019-11-20">2019-11-20</time> [<svg class="icon" viewbox="0 0 72 24"><desc>Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</desc><use href="./assets/icons.svg#cc-by-nc-sa"/></svg>][cc-by-nc-sa]</small>

<div class="soc-icons">

- [<svg class="icon" viewbox="0 0 24 24"><desc>Mastodon</desc><use href="./assets/icons.svg#social-mastodon"/></svg> @toastal\@cybre.space][mastodon]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Twitter</desc><use href="./assets/icons.svg#social-twitter"/></svg> toastal][twitter]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Keybase</desc><use href="./assets/icons.svg#social-keybase"/></svg> toastal][keybase]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Flickr</desc><use href="./assets/icons.svg#social-flickr"/></svg> toastal][flickr]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Gitlab</desc><use href="./assets/icons.svg#social-gitlab"/></svg> toastal][gitlab]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Github</desc><use href="./assets/icons.svg#social-github"/></svg> toastal][github]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Steam</desc><use href="./assets/icons.svg#social-steam"/></svg> toastal][steam]

</div>

[cc-by-nc-sa]: https://creativecommons.org/licenses/by-nc-sa/4.0/ "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International"
[flickr]: https://www.flickr.com/photos/toastal/
[github]: https://github.com/toastal
[gitlab]: https://gitlab.com/toastal
[keybase]: https://keybase.io/toastal
[mastodon]: https://cybre.space/@toastal
[purescript]: http://www.purescript.org/
[steam]: https://steamcommunity.com/id/toastal
[twitter]: https://twitter.com/toastal

- - -

#### Special thanks


I reached out to Justin Woo, [<svg class="icon" viewbox="0 0 24 24"><desc>Twitter</desc><use href="./assets/icons.svg#social-twitter"></svg> @jusrin00](https://twitter.com/toastal), for some assistance and borrowed (or stolen) many of his ideas. Also his libraries. Also community activism.

- - -

#### About me

<div class="about-me">

- ผมเป็นคนอเมริกา
- อยู่เมืองไทย1.5ปี
- เป็นโปรแกรมเมอร์ประมาณ9ปี เคยใช้ภาษา PHP, JavaScript, Python, Elm, ClojureScript, PureScript ที่ทำงาน
- อยากเรียนพูดภาษาไทยเก่งขึ้น แล้วภาษาลาวด้วย

- I’m American
- I’ve been in Thailand for 1.5 years
- I’ve been a programmer for about 9 years having done PHP, JavaScript, Python, Elm, ClojureScript, PureScript professionally
- I want to learn to speak Thai better, as well as Lao

</div>


* * *


## What’s the Purpose Today

- What is PureScript?
- What does PureScript look like?
- How can we use this PureScript’s type system to make rows?
- What is cool about these rows things and how can they be applied?


* * *


# Ready
# พร้อม

- - -

# Set
# ระวัง

- - -

# Row
# พาย

- - -

![Chip gap Dale](./assets/chip-dale-rowboat.jpg)

- - -

![Me corn pie](./assets/me-pie.jpg)


* * *


### What is PureScript?
###### It’s a strongly-typed, purely-functional programming language that compiles to JavaScript*

- Persistent data structures
- Algebraic data types (ADTs)
- Pattern matching
- Higher-kinded types & rank n types
- Type classes + functional dependencies
- Type inference
- Syntax similar to Haskell
- Can compile to C++, Go, Erlang (code reuse)

- - -

### PureScript compared to Haskell?

- Strict evaluation (not lazy)
- Type class hierachy cleaned up <small>(`Apply`, `Semigroup` vs. `Monoid`, `pure` vs. `return`, etc.)</small>
- Spago uses Dhall for package management
- PSCID is magic
- Record syntax, `ado` for applicative do
- Row types are first-class

<small>(with negatives)</small>

- - -

### PureScript compared to Elm?

- FFI (foreign function interface) is much more flexible, including synchronous operations
- Can express things more abstractly (no real polymorphism)
- Alternative ways to do things
- Option to write it on the back-end
- More row things

<small>(with negatives)</small>

- - -

### PureScript compared to BuckleScript/Reason?

- OCaml & friends are not _pure_ functional; IO can happen anywhere
- Not object-oriented (meaning?); inhertance isn’t a part of PureScript
- I’m going to assume row things (based on what I read)

<small>I’ve not done anything beyond `hello world` in my own time</small>


* * *


## Whirlwind Syntax

- - -

```haskell
module Main (main) where

import Prelude

import Effect (Effect)
import Effect.Console (log)

main :: Effect Unit
main = log "Hello World!"
```

- - -

```haskell
import Effect.Exception.Unsafe (unsafeThrow)

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)

factorial2 :: Int -> Int
factorial2
  | n < 0     = unsafeThrow "DON’T DO THIS ON PROD"
  | n == 0    = 1
  | otherwise = n * factorial2 (n - 1)
```

- - -

```haskell
data List a = Nil | Cons a (List a)

infixr 6 Cons as :

list :: List Int
list = 3 : 2 : 1 : Nil
-- Cons 3 (Cons 2 (Cons 1 Nil))

map :: forall a b. (a -> b) -> List a -> List b
map fn l = case l of
  Nil -> Nil
  Cons x xs -> fn x : map xs -- although no automatic TCO

plus1 :: List Int
plus1 = map p1 list
  where
  p1 :: Int -> Int -> Int
  p1 = _ + 1
-- Cons 4 (Cons 3 (Cons 2 Nil))
```

- - -

```haskell
aFooBar :: { foo :: String, bar :: Int }
aFooBar =
  { foo: "foo", bar: 1 }

type MyFooBar =
  { foo :: String, bar :: Int }

bFooBar :: MyFooBar
bFooBar =
  { foo: "oof", bar: -1 }

exclaimFoo :: MyFooBar -> String
exclaimFoo r =
  r.foo <> "!"
-- exclaimFoo bFooBar == "oof!"

-- talk about this more later
```


* * *


## Type Classes + Kinds Flyby

<small>Much will be taken from [@jusrin00’s workshop](https://jusrin.dev/real-world-purescript-workshop-2019/day1.html) because Elm lacks type classes so I don’t use them daily and when I do, I don’t really create my own.</small>

- - -

Given some concrete type, you should be able to match some instance, which may lead to

1. additional types being determined
2. additional functions being determined


```haskell
-- | + -> tells PureScript element is determined container
class Unwrap container element | container -> element where
  unwrap :: container -> element

data Money = THB Int

instance unwrapMoneyInstance :: Unwrap Money Int where
  unwrap = case _ of
    THB a -> a
```

<small>[source](https://jusrin.dev/real-world-purescript-workshop-2019/day1.html#10) + [more info](https://wiki.haskell.org/Functional_dependencies)</small>

- - -

```haskell
class Switcheroo input output | input -> output

instance switchIntString :: Switcheroo Int String
instance switchStringBoolean :: Switcheroo String Boolean

-- Phantom type (https://wiki.haskell.org/Phantom_type)
data Proxy s = Proxy

switcheroo
  :: forall input output
   . Switcheroo input output
  => Proxy input -> Proxy output
switcheroo _ = Proxy

switchResult1 :: Proxy String
switchResult1 = switcheroo (Proxy :: Proxy Int)

switchResult2 :: Proxy ?hole
switchResult2 = switcheroo (Proxy :: Proxy String)
-- Using PureScript’s type holes:
-- Hole 'hole' has the inferred type Boolean
```

<!-- the RodeoType name wasn't clear to me nor did it resonate -->
<small>[source](https://jusrin.dev/real-world-purescript-workshop-2019/day1.html#11)</small>

- - -

```haskell
data Proxy (s :: Type) = Proxy
```

<div style="font-size:0.82em">

`Proxy` ’s kind is inferred by the compiler. Regular `Type` kind always has a value, but not it’s not a requirement. `Symbol` does not (used a lot working with records & rows).

</div>

```haskell
-- from Data.Symbol
data SProxy (sym :: Symbol) = SProxy

-- Information at the type level is simply used in compile time:
staticStringProxy :: SProxy "hello world"
staticStringProxy = SProxy

-- from Data.Symbol
class IsSymbol (sym :: Symbol) where
  reflectSymbol :: SProxy sym -> String

-- But that information can be plucked out
reflectedSymbolValue :: String
reflectedSymbolValue = reflectSymbol staticStringProxy
```

<small>[source](https://jusrin.dev/real-world-purescript-workshop-2019/day1.html#12)</small>

- - -

### Arrows as basic kinds

```haskell
type IntToBool = Int -> Bool

divisibleBy2 :: IntToBool
divisibleBy2 t = mod 2 t == 0

type Arrow = (->) :: Type -> Type -> Type
type FromInt = (->) Int :: Type -> Type
type IntToBool' = FromInt Bool :: Type
-- equivalent to the first declaration
```

<small>[source](https://jusrin.dev/real-world-purescript-workshop-2019/day1.html#13)</small>



* * *


### Records

```haskell
data Record :: # Type -> Type

type Pai = { pie :: String }

-- equivalent to this ↓ closed record

type Pai = Record ( pai :: String )

makeApple :: Pai -> Pai
makeApple r = r { pie = "แอปเปิ้ล" }

getPie :: forall r. { pie :: String | r } -> String
getPie r = r.pie
```

- - -

Unlike Haskell where the records are sugar for sum types, PureScript’s records are rows.

- - -

### Row type classes

Using these row type parameters from [`Prim.Row`](https://pursuit.purescript.org/builtins/docs/Prim.Row)

```haskell
class Union (left :: # Type) (right :: # Type) (union :: # Type)
  | left right -> union
  , right union -> left
  , union left -> right

class Nub (original :: # Type) (nubbed :: # Type)
  | original -> nubbed

class Lacks (label :: Symbol) (row :: # Type)

class Cons (label :: Symbol) (a :: Type)
           (tail :: # Type) (row :: # Type)
  | label a tail -> row
  , label row -> a tail
```

- - -

```haskell
get :: forall r r' l a. IsSymbol l => Cons l a r' r =>
  SProxy l -> Record r -> a
get = -- handwaving, implementation not important

result :: ?hole
result = get (SProxy :: _ "pie") { pie: "สัปปะรด" }
-- Using PureScript’s type holes:
-- Hole 'hole' has the inferred type String
```

- - -

##### [`Record`](https://pursuit.purescript.org/packages/purescript-record/2.0.1)

<div class="record">

```haskell
-- unicode to save space; `∀` is an alias for `forall`
get ∷ ∀ r r' l a. IsSymbol l ⇒ Cons l a r' r ⇒
  SProxy l → Record r → a

set ∷ ∀ r1 r2 r l a b. IsSymbol l ⇒ Cons l a r r1 ⇒ Cons l b r r2 ⇒
  SProxy l → b → Record r1 → Record r2

modify ∷ ∀ r1 r2 r l a b. IsSymbol l ⇒ Cons l a r r1 ⇒ Cons l b r r2 ⇒
  SProxy l → (a → b) → Record r1 → Record r2

insert ∷ ∀ r1 r2 l a. IsSymbol l ⇒ Lacks l r1 ⇒ Cons l a r1 r2 ⇒
  SProxy l → a → Record r1 → Record r2

delete ∷ ∀ r1 r2 l a. IsSymbol l ⇒ Lacks l r1 ⇒ Cons l a r1 r2 ⇒
  SProxy l → Record r2 → Record r1

rename ∷ ∀ prev next ty input inter output. IsSymbol prev ⇒ IsSymbol next ⇒ Cons prev ty inter input ⇒ Lacks prev inter ⇒ Cons next ty inter output ⇒ Lacks next inter ⇒
  SProxy prev → SProxy next → Record input → Record output

equal ∷ ∀ r rs. RowToList r rs ⇒ EqualFields rs r ⇒
  Record r → Record r → Boolean

merge ∷ ∀ r1 r2 r3 r4. Union r1 r2 r3 ⇒ Nub r3 r4 ⇒
  Record r1 → Record r2 → Record r4

union ∷ ∀ r1 r2 r3. Union r1 r2 r3 ⇒
  Record r1 → Record r2 → Record r3

disjointUnion ∷ ∀ r1 r2 r3. Union r1 r2 r3 ⇒ Nub r3 r3 ⇒
  Record r1 → Record r2 → Record r3

nub ∷ ∀ r1 r2. Nub r1 r2 ⇒
  Record r1 → Record r2
```

</div>

- - -

### Rows also go to lists (so have fun LISPers)

#### [`Prim.RowList`](https://pursuit.purescript.org/builtins/docs/Prim.RowList)
```haskell
kind RowList

data Cons :: Symbol -> Type -> RowList -> RowList

data Nil :: RowList

class RowToList (row :: # Type) (list :: RowList)
  | row -> list
````

- - -

## If you’re still not getting it, then take this analogy
#### (with a grain of salt)

- - -

> A row of types represents an unordered collection of named types, with duplicates. Duplicate labels have their types collected together in order, as if in a NonEmptyList. This means that, conceptually, a row can be thought of as a type-level `Map Label (NonEmptyList Type)`


* * *


#### Places rows can take you:

- Record building with less overhead using `Builder`: [records](https://pursuit.purescript.org/packages/purescript-record)
- Polymorphic variants: [variants](https://pursuit.purescript.org/packages/purescript-variant)
- Errors: [chirashi](https://github.com/justinwoo/purescript-chirashi)
- JSON parsing/deriving: [simple-json](https://purescript-simple-json.readthedocs.io/en/latest/)
- INI parsing/deriving: [tortellini](https://pursuit.purescript.org/packages/purescript-tortellini)
- Extensible effects: [run](https://pursuit.purescript.org/packages/purescript-run)
- Extensible coeffects using comonads & Day convolution(?): [smash](https://pursuit.purescript.org/packages/purescript-smash)
- & more…


* * *


## Thanks! ขอบคุณหลายๆ

#### Try it!: [www.purescript.org/][purescript]

<div class="soc-icons">

- [<svg class="icon" viewbox="0 0 24 24"><desc>Mastodon</desc><use href="./assets/icons.svg#social-mastodon"/></svg> @toastal\@cybre.space][mastodon]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Twitter</desc><use href="./assets/icons.svg#social-twitter"/></svg> toastal][twitter]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Keybase</desc><use href="./assets/icons.svg#social-keybase"/></svg> toastal][keybase]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Flickr</desc><use href="./assets/icons.svg#social-flickr"/></svg> toastal][flickr]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Gitlab</desc><use href="./assets/icons.svg#social-gitlab"/></svg> toastal][gitlab]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Github</desc><use href="./assets/icons.svg#social-github"/></svg> toastal][github]
- [<svg class="icon" viewbox="0 0 24 24"><desc>Steam</desc><use href="./assets/icons.svg#social-steam"/></svg> toastal][steam]

</div>


[flickr]: https://www.flickr.com/photos/toastal/
[github]: https://github.com/toastal
[gitlab]: https://gitlab.com/toastal
[keybase]: https://keybase.io/toastal
[mastodon]: https://cybre.space/@toastal
[purescript]: http://www.purescript.org/
[steam]: https://steamcommunity.com/id/toastal
[twitter]: https://twitter.com/toastal

<!-- "hack" to get this file loaded by the static generator -->
<div style="display:none">![hack](./assets/icons.svg)</div>
